package com.sap.gateway.dto;

/**
 * The data transfer object used to hold parsed object
 *  which are ready to send.
 * @author Nihar
 *
 */
public class MessageDTO {

	private int port;
	private Object message;
	
	//getter & setter
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public Object getMessage() {
		return message;
	}
	public void setMessage(Object message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "port=" + port + ", message=" + message;
	}
	
	
	
}
