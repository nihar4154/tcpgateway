package com.sap.gateway.loadbalancing;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * The Service will check the message format and push to respective queues. 
 * 
 * @author Nihar
 *
 */
public class LoadBalancingService {

	public void service(Object data) {
		if (data instanceof JSONObject) {
			JSONObject jSONObject = (JSONObject) data;
			String clientType = jSONObject.get("type").toString().toLowerCase();
			if (clientType.equals("green")) {
				LoadBalancingQueue.pushTOGreenQueue(data);
			} else if (clientType.equals("blue")) {
				LoadBalancingQueue.pushTOBlueQueue(data);
			} else {
				System.out.println("Wrong Input " + clientType);
			}
		} else if (data instanceof String) { // xml check
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			String clientType = null;
			try {
				builder = factory.newDocumentBuilder();
				InputSource is = new InputSource(new StringReader((String) data));
				Document document = builder.parse(is);
				document.getDocumentElement().normalize();
				clientType = document.getElementsByTagName("type").item(0).getTextContent().toLowerCase();
			} catch (ParserConfigurationException | SAXException | IOException e) {
				e.printStackTrace();
			}
			

			if (clientType != null && clientType.equals("green")) {
				LoadBalancingQueue.pushTOGreenQueue(data);
			} else if (clientType != null && clientType.equals("blue")) {
				LoadBalancingQueue.pushTOBlueQueue(data);
			} else {
				System.out.println("Wrong Input " + clientType);
			}
		} else {
			System.out.println("Invalid request:");
		}
	}
}
