package com.sap.gateway.loadbalancing;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;

/**
 * Responsible for reading data from multiple clients 
 * @author Nihar
 *
 */
public class EchoThread extends Thread {
	protected Socket socket;
	
	LoadBalancingService loadBalancingService = new LoadBalancingService();

	public EchoThread(Socket clientSocket) {
		this.socket = clientSocket;
	}

	public void run() {
		InputStream inp = null;
		ObjectInputStream objectInputStream = null;
		DataOutputStream out = null;
		try {
			inp = socket.getInputStream();
			objectInputStream = new ObjectInputStream(inp);
			out = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			return;
		}
		Object data;
		while (true) {
			try {
				data = objectInputStream.readObject();
				loadBalancingService.service(data);
				if ((data == null)) {
					socket.close();
					return;
				} else {
					out.writeBytes(data+ "\n\r");
					out.flush();
				}
			} catch (IOException | ClassNotFoundException e) {
				return;
			}
		}
	}
}
