package com.sap.gateway.loadbalancing;

import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import com.sap.gateway.util.PropertiesReader;

/**
 * The Load Balancer class used to push two different 
 * types(green/blue) of request object into respective queues.
 * @author Nihar
 *
 */
public class LoadBalancingQueue {

	//to hold all green types of objects
	private static BlockingQueue<Object> greenQueue;
	//to hold all blue types of objects
	private static BlockingQueue<Object> blueQueue;

	static volatile int gcount = 1;
	static volatile int bcount = 1;

	ExecutorService myExecutor = Executors.newCachedThreadPool();

	static {
		greenQueue = new LinkedBlockingQueue<Object>(100);
		blueQueue = new LinkedBlockingQueue<Object>(100);
	}

	//push to green queue
	public static synchronized boolean pushTOGreenQueue(Object o) {
		try {
			greenQueue.put(o);
			return true;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}

	//push to blue queue
	public static synchronized boolean pushTOBlueQueue(Object o) {
		try {
			blueQueue.put(o);
			return true;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}

	//The background thread reads top from two queues. 
	//And making Asynchronous calls 
	public void startService() {
		Thread t = new Thread(new Runnable() {
			public void run() {
				while (true) {
					if (greenQueue.size() > 0) {
						myExecutor.execute(new Runnable() {
							public void run() {
								try {
									send(greenQueue.take(),
											Integer.parseInt(PropertiesReader.getProperty("gateway.clientport")));
								} catch (NumberFormatException | InterruptedException e) {
									e.printStackTrace();
								}
							}
						});
					}
					if (blueQueue.size() > 0) {
						myExecutor.execute(new Runnable() {
							public void run() {
								try {
									send(blueQueue.take(),
											Integer.parseInt(PropertiesReader.getProperty("gateway.serverport")));
								} catch (NumberFormatException | InterruptedException e) {
									e.printStackTrace();
								}
							}
						});
					}
				}
			}
		});
		t.start();
	};

	// send data 
	private static void send(Object data, int port) {
		try {
			Socket socket = new Socket("localhost", port);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
			objectOutputStream.writeObject(data);
			objectOutputStream.close();
			socket.close();
		} catch (Exception e) {
			return;
		}
	}

}
