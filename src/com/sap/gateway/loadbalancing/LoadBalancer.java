package com.sap.gateway.loadbalancing;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The single entry point for all the request.
 * Initialize Load balancer and start listening 
 * @author Nihar
 *
 */
public class LoadBalancer {
	static int count = 0;
	LoadBalancingService loadBalancingService = null;

	public void start(final int port) {
		loadBalancingService = new LoadBalancingService();
		Thread t = new Thread(new Runnable() {
			public void run() {
				System.out.println("Load Balancer started: " + port);
				listen(port);
			}
		});
		t.start();
	}

	private void listen(final int port) {
		ServerSocket serverSocket = null;
		Socket socket = null;
		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			return;

		}
		while (true) {
			try {
				socket = serverSocket.accept();
			} catch (IOException e) {
				System.out.println("I/O error: " + e);
			}
			new EchoThread(socket).start();
		}
	}
}
