package com.sap.gateway.service;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sap.gateway.dto.MessageDTO;
import com.sap.gateway.queue.MessageQueue;
import com.sap.gateway.util.PropertiesReader;

/**
 * The Routing service helps to check client message format, 
 * verify destination format from property file, 
 * convert to target format and push into Queue. 
 * @author Nihar
 *
 */
public class RoutingService {

	public void route(Object message) {

		if (message instanceof JSONObject) {
			MessageDTO dto = parseJSONMessage((JSONObject) message);
			boolean b = MessageQueue.pushMessage(dto);

		} else if (message instanceof String) {
				MessageDTO dto = parseXMLMessage(message.toString());
				boolean b = MessageQueue.pushMessage(dto);
		} else {
			throw new RuntimeException("invalid request...");
		}
	}

	// parseXMLMessage
	private static MessageDTO parseXMLMessage(String stringData) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(stringData));
			Document document = builder.parse(is);
			document.getDocumentElement().normalize();
			String clientName = document.getElementsByTagName("to").item(0).getTextContent().toLowerCase();
			String _path = "client." + clientName + ".message.format";
			String _destPortPath = "client.";
			_destPortPath += clientName;
			_destPortPath +=".port";
			final String messageFormat = PropertiesReader.getProperty(_path);
			String destPort = PropertiesReader.getProperty(_destPortPath);

			MessageDTO messageDTO = new MessageDTO();
			messageDTO.setPort(Integer.parseInt(destPort));
			if (messageFormat.equals("JSON")) {
				JSONObject obj = new JSONObject();
				obj.put("from", document.getElementsByTagName("from").item(0).getTextContent());
				obj.put("to", clientName);
				obj.put("message", document.getElementsByTagName("message").item(0).getTextContent());
				messageDTO.setMessage(obj);
				return messageDTO;

			} else if (messageFormat.equals("XML")) {
				messageDTO.setMessage(stringData);
				return messageDTO;
			} else {
				throw new RuntimeException("Destination client not found");
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
			throw new RuntimeException("Invalid XML format");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static MessageDTO parseJSONMessage(JSONObject jsonData) {
		try {
			String clientName = jsonData.get("to").toString().toLowerCase();
			final String propMsgFormatPath = "client." + clientName + ".message.format";
			final String messageFormat = PropertiesReader.getProperty(propMsgFormatPath);
			final String portPath = "client." + clientName + ".port";
			
			String port = PropertiesReader.getProperty(portPath);
			MessageDTO dto = new MessageDTO();
			dto.setPort(Integer.parseInt(port)); // port

			if (messageFormat.equals("JSON")) {
				dto.setMessage(jsonData); // send same json object
				return dto;

			} else if (messageFormat.equals("XML")) {
				String xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-15\"?>\n<messages>"
						+ org.json.XML.toString(jsonData) + "</messages>";
				dto.setMessage(xml);
			} else {
				throw new RuntimeException("Requested Destination not found");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ex);
		}

		return null;
	}

}
