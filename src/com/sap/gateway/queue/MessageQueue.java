package com.sap.gateway.queue;

import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.json.simple.JSONObject;

import com.sap.gateway.dto.MessageDTO;

/**
 * The Queue holds all parsed data which are ready to send
 * The background thread reads from queue 
 * and send data over socket in destination message format.
 * @author Nihar
 *
 */
public class MessageQueue {

	private static BlockingQueue<MessageDTO> queue;
	static {
		queue = new LinkedBlockingQueue<MessageDTO>();
	}

	public static boolean pushMessage(MessageDTO dto) {
		boolean isAdded = queue.add(dto);
		return isAdded;
	}

	// background thread sending data from queue to client
	public static void startService() {
		Thread t = new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {

						new MessageQueue().sendMessage(queue.take());
					} catch (InterruptedException ex) {
						System.out.println(ex);
					}
				}
			}
		});
		t.start();
	}

	private void sendMessage(MessageDTO messageDTO) {
		try {
			 System.out.println("sending:" +messageDTO.toString());
			Socket socket = new Socket("localhost", messageDTO.getPort());
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
			if (messageDTO.getMessage() instanceof JSONObject)
				objectOutputStream.writeObject((JSONObject) messageDTO.getMessage());
			else
				objectOutputStream.writeObject((String) messageDTO.getMessage());
			objectOutputStream.close();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			System.exit(0);
		}
	}
}
