package com.sap.gateway;

import com.sap.gateway.listeners.ClientListener;
import com.sap.gateway.loadbalancing.LoadBalancer;
import com.sap.gateway.loadbalancing.LoadBalancingQueue;
import com.sap.gateway.queue.MessageQueue;
import com.sap.gateway.util.PropertiesReader;

/**
 * main class (starting point of the gateway)
 * 
 * It will start client listener(8081) and server listener(9090).
 * And the Load balancer Listener
 * Also start the message queue for sending message
 * @author Nihar
 *
 */
public class StartGateway {

	public static void main(String[] args) {

		new ClientListener().listenPort(Integer.parseInt(PropertiesReader.getProperty("gateway.clientport")));
		new ClientListener().listenPort(Integer.parseInt(PropertiesReader.getProperty("gateway.serverport")));
		new LoadBalancer().start(Integer.parseInt(PropertiesReader.getProperty("gateway.loadbalancerport")));
		MessageQueue.startService();
		new LoadBalancingQueue().startService();
	}
}
