/**
 * The Listener class for port 8181
 */

package com.sap.gateway.listeners;

import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

import com.sap.gateway.service.RoutingService;

/**
 * The Listener class
 * @author Nihar
 *
 */
public class ClientListener {

	private static RoutingService routingService;

	public void listenPort(final int port) {
		Thread t = new Thread(new Runnable() {
			public void run() {
				System.out.println("started Listener at port: " + port);
				while (true) {
					try {
						ServerSocket ss = new ServerSocket(port);
						Socket s = ss.accept();// establishes connection
						ObjectInputStream objectInputStream = new ObjectInputStream(s.getInputStream());
						Object data = objectInputStream.readObject();
						routingService = new RoutingService();
						routingService.route(data);
						objectInputStream.close();
						ss.close();
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e);
						System.exit(0);
					}
				}
			}
		});
		t.start();
	}
}
