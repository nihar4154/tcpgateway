package com.sap.gateway.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Read property value from file
 * @author Nihar
 *
 */
public class PropertiesReader {

	static Properties prop;
	
	static{
		prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("resources/config.properties");
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String getProperty(String key) {
		return prop.getProperty(key);
	}

}
