package com.sap.gateway.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Util {

	// utility method to read json from file (to know destination format)
	public static JSONObject getJSONFormat() {
		
		JSONObject jSONObject = null;
		JSONParser parser = new JSONParser();
		
		try {
			Object obj= parser.parse(new FileReader("resources/json_message_format.json"));
			jSONObject = (JSONObject) obj;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return jSONObject;
		
	}
}
