Assumptions:
Based on the above requirement, below are the assumptions which are taken care for the implementation.
•	The good implementation can be done by using various third party libraries but as per my understanding with the requirement, created simple implementation with maximum manual approach for coding. For example basic socket programming, thread pool, multithreading, BlockingQueue etc.
•	The third party libraries are only used only in case of parsing different types of messages format.
•	Below is the high level architecture diagram.

High Level diagram:
  ![diagram.PNG](https://bitbucket.org/repo/7EzK8K9/images/3184746734-diagram.PNG)

•	As per the requirement, two different types (for e.g green and blue) of clients send message to 8080 and 9090 respectively. Each client have different message format.  I have only taken JSON and XML as of now. But we can support many format easily.
•	For the load balancing, can be implemented by Nginx library for TCP environment but here I have implemented manually to meet the requirement.

Implementation:

1.	As per the above diagram, LoadBalancer has the single entry point for all clients. Load balancer will read the data and push into the respective queue based on types. A background job will check the availability from two queues. It will check the load balancing condition and start two asynchronous threads for 8080 and 9090.

2.	The Listener will parse the data and forward to Router service for checking destination format from property file. The message converter will convert the message and will be pushed to Message queue. 

3.	The background thread will read the message queue ( contains DTO objects), get the message and destination port and send to the client.

4.	As of now I have only taken four clients but new clients can be added easily by making a new entry in property file.

5.	Please note that I have not included test case because of the time constraint.

Environment: Maven, git, JDK1.8
How to Run:

•	Start the gateway.  Run main method of  com.sap.gateway. StartGateway
•	Start each client individually. B1.java, B2.java, G1.java, G2.java
•	 To increase the load increase the for loop condition in clients.